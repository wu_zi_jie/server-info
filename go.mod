module gitee.com/wu_zi_jie/server-info

go 1.16

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/gin-gonic/gin v1.8.1
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/shirou/gopsutil v3.21.9+incompatible
	github.com/tklauser/go-sysconf v0.3.9 // indirect
)
