获取服务器详情信息的json结果
```markdown
参数解读 
goos: 操作系统 
numCpu: CPU逻辑处理器数
compiler: GCC编译器
goVersion: golang版本
numGoroutine: 当前goroutine数量
cpus: 各CPU核心使用情况
cores: CPU内核
ram:内存
    usedMb: 使用内存数MB
    totalMb: 总内存大小MB
    usedPercent: 内存使用占比
disk
    usedMb: 当前磁盘使用MB
    usedGb: 当前磁盘使用GB
    totalMb: 当前磁盘总MB
    totalGb: 当前磁盘总GB
    usedPercent: 当前磁盘使用占比
```
```json
{
  "os": {
    "goos": "windows",
    "numCpu": 16,
    "compiler": "gc",
    "goVersion": "go1.16.2",
    "numGoroutine": 1
  },
  "cpu": {
    "cpus": [
      0,
      0,
      7.142857142857142,
      0,
      21.428571428571427,
      0,
      21.428571428571427,
      0,
      0,
      0,
      0,
      7.142857142857142,
      0,
      0,
      7.142857142857142,
      7.142857142857142
    ],
    "cores": 8
  },
  "ram": {
    "usedMb": 10514,
    "totalMb": 32678,
    "usedPercent": 32
  },
  "disk": {
    "usedMb": 188519,
    "usedGb": 184,
    "totalMb": 339465,
    "totalGb": 331,
    "usedPercent": 55
  }
}
```