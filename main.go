package main

import (
	"encoding/json"
	"fmt"
	"gitee.com/wu_zi_jie/server-info/utils"
	"github.com/gin-gonic/gin"
)

func main() {
	engine := gin.Default()
	var s utils.Server
	s.Os = utils.InitOS()
	s.Cpu, _ = utils.InitCPU()
	s.Rrm, _ = utils.InitRAM()
	s.Disk, _ = utils.InitDisk()
	//fmt.Printf("%#v \n", s)
	jsonByte, _ := json.Marshal(s)
	jsonStr := string(jsonByte)
	fmt.Printf("%v", jsonStr)
	engine.GET("/getInfo", func(c *gin.Context) {
		c.JSON(200, s)
	})
	engine.GET("/getNums", func(c *gin.Context) {
		c.JSON(200, "No.1!!!!")
	})
	engine.Run("0.0.0.0:2333")
}
